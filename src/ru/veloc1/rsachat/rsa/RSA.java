package ru.veloc1.rsachat.rsa;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import ru.veloc1.rsachat.ChatApplication;
import ru.veloc1.rsachat.rsa.lowlevel.BigIntegerHelper;
import ru.veloc1.rsachat.rsa.lowlevel.KeyStorage;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


public enum RSA {
	INSTANCE;

	private static final int	KEYLENGTH	= 1024;

	public void generateAndSaveKeys() {
		Key myKey = RsaHelper.createKeys(KEYLENGTH, ChatApplication.getSelfUser());
		KeyStorage.INSTANCE.saveKey(myKey);
	}

	public String cypher(String message, String user) {
		if (message.length() == 0)
			return "";
		Key key = KeyStorage.INSTANCE.restoreKey(user);
		List<String> chunks = splitEqually(message, KEYLENGTH / 2 / 4); // Простые числа длиной KEYLENGTH / 2. Делим на 4 байта из-за
																		// кодировки utf-32
		String cypher = "";
		for (String chunk : chunks) {
			BigInteger m = BigIntegerHelper.toBigInteger(chunk);
			m = BigIntegerHelper.modPower(m, key.getPublicPart(), key.getCommonPart());
			cypher = cypher.concat(m.toString().concat(" "));
		}
		return cypher.trim();
	}

	public String decypher(String cypher) {
		if (cypher.length() == 0)
			return "";
		Key key = KeyStorage.INSTANCE.restoreKey(ChatApplication.getSelfUser());
		String decypher = "";
		try {
			BigInteger m = null;
			String[] chunks = cypher.split(" ");
			for (String chunk : chunks) {
				m = new BigInteger(chunk);
				m = BigIntegerHelper.modPower(m, key.getPrivatePart(), key.getCommonPart());
				decypher = decypher.concat(BigIntegerHelper.toString(m));
			}
		}
		catch (Exception e) {
			return cypher;
		}
		return decypher;
	}

	public List<String> splitEqually(String text, int size) {
		List<String> ret = new ArrayList<String>((text.length() + size - 1) / size);

		for (int start = 0; start < text.length(); start += size) {
			ret.add(text.substring(start, Math.min(text.length(), start + size)));
		}
		return ret;
	}

	@DatabaseTable(tableName = "keys")
	public static class Key implements Serializable {

		private static final long	serialVersionUID	= 569700118494812436L;

		public static final String	DB_USER				= "user";

		@DatabaseField(generatedId = true)
		private int					id;
		@DatabaseField
		private BigInteger			mPublicPart;
		@DatabaseField
		private BigInteger			mPrivatePart;
		@DatabaseField
		private BigInteger			mCommonPart;
		@DatabaseField(columnName = DB_USER)
		private String				mUser;

		public String getUser() {
			return mUser;
		}

		public void setUser(String user) {
			mUser = user;
		}

		public Key() {
			mCommonPart = null;
			mPrivatePart = null;
			mPublicPart = null;
		}

		public BigInteger getPublicPart() {
			return mPublicPart;
		}

		public void setPublicPart(BigInteger publicPart) {
			mPublicPart = publicPart;
		}

		public BigInteger getPrivatePart() {
			return mPrivatePart;
		}

		public void setPrivatePart(BigInteger privatePart) {
			mPrivatePart = privatePart;
		}

		public BigInteger getCommonPart() {
			return mCommonPart;
		}

		public void setCommonPart(BigInteger commonPart) {
			mCommonPart = commonPart;
		}

		@Override
		public String toString() {
			return "Public part = ".concat(mPublicPart.toString()).concat(
					"\nPrivate part = ".concat(mPrivatePart.toString()).concat("\nCommon part = ".concat(mCommonPart.toString())));
		}

	}
}
