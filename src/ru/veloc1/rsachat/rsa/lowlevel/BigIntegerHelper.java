package ru.veloc1.rsachat.rsa.lowlevel;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;

import android.annotation.TargetApi;
import android.os.Build;


public class BigIntegerHelper {

	private static SecureRandom	mRandomGenerator	= new SecureRandom();

	/**
	 * Возвращает true если число возможно простое. Используем тесты, основанные на малой теореме Ферма.
	 * 
	 * @param bigInt
	 *            Число, которое нужно проверить на простоту
	 * @param testCount
	 *            количество тестов
	 */
	public static boolean isPrime(BigInteger bigInt, int testCount) {
		for (int k = 0; k < testCount; k++) {
			int r = mRandomGenerator.nextInt();
			BigInteger a = new BigInteger("" + r);
			if (!(modPower(a, bigInt.subtract(BigInteger.ONE), bigInt).equals(BigInteger.ONE))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return the multiplicative inverse of a mod n. Precondition: the inverse exists.
	 * 
	 * @param a
	 *            the value to find the inverse of
	 * @param n
	 *            the modulus
	 * @return the inverse of a, mod n
	 */
	public static BigInteger inverse(BigInteger a, BigInteger n) {
		BigInteger[] rtn = extendedEuclid(a, n);
		if (!rtn[0].equals(BigInteger.ONE)) {
			// reportError("Arguments (" + a + "," + n + ") to inverse aren't relatively prime.");
		}
		if (rtn[1].compareTo(BigInteger.ZERO) == 1) {
			return rtn[1];
		} else {
			return rtn[1].add(n);
		}
	}

	/**
	 * Return a triple (d, x, y) in which d, x, and y are BigIntegers, d = gcd(a,b) and a*x + b*y = d. Use the Extended Euclid to determine
	 * these values.
	 * 
	 * @param a
	 * @param b
	 *            the BigInteger arguments whose gcd is to be determined
	 * @return the triple (d,x,y) of BigIntegers in which d = gcd(a,b) and a*x + b*y = d.
	 */
	private static BigInteger[] extendedEuclid(BigInteger a, BigInteger b) {
		BigInteger[] rtn = new BigInteger[3];
		if (b.equals(BigInteger.ZERO)) {
			rtn[0] = a;
			rtn[1] = BigInteger.ONE;
			rtn[2] = BigInteger.ZERO;
			return rtn;
		}
		rtn = extendedEuclid(b, a.mod(b));
		BigInteger x = rtn[1];
		BigInteger y = rtn[2];
		rtn[1] = y;
		rtn[2] = x.subtract(y.multiply(a.divide(b)));
		return rtn;
	}

	/**
	 * Возвращает результат остатка от деления по модулю n, числа m, возведенного в степень exp.
	 */
	public static BigInteger modPower(BigInteger m, BigInteger exp, BigInteger n) {
		BigInteger rtn = BigInteger.ONE;
		while (exp.signum() > 0) {
			if (exp.testBit(0)) {
				rtn = rtn.multiply(m).mod(n);
			}
			exp = exp.shiftRight(1);
			m = m.multiply(m).mod(n);
		}
		return rtn;
	}

	/**
	 * Конвертирование строки в BigInteger, используя символы ascii как цифры.
	 */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public static BigInteger toBigInteger(String str) {
//		byte[] asciiValues = str.getBytes(Charset.defaultCharset());
//		for (Charset  c : Charset.availableCharsets().values()) {
//			Log.e("test", c.displayName());
//		}
		byte[] asciiValues = str.getBytes(Charset.forName("UTF-32LE"));
		return new BigInteger(asciiValues);
	}

	/**
	 * Конвертирование BigInteger в ascii-строку.
	 */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public static String toString(BigInteger bigInt) {
		byte[] bytes = bigInt.toByteArray();
//		return new String(bytes, Charset.defaultCharset());
		return new String(bytes, Charset.forName("UTF-32LE"));
	}
}
