package ru.veloc1.rsachat.rsa.lowlevel;

import ru.veloc1.rsachat.db.DatabaseManager;
import ru.veloc1.rsachat.rsa.RSA.Key;


public enum KeyStorage {
	INSTANCE;

	public void saveKey(Key key) {
		DatabaseManager.INSTANCE.add(key, Key.class);
	}

	public Key restoreKey(String user) {
		return DatabaseManager.INSTANCE.getKeyByUser(user);
	}
	
}
