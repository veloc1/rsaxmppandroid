package ru.veloc1.rsachat.rsa;

import java.math.BigInteger;
import java.security.SecureRandom;

import ru.veloc1.rsachat.rsa.RSA.Key;
import ru.veloc1.rsachat.rsa.lowlevel.BigIntegerHelper;
import android.util.Log;


public class RsaHelper {

	/**
	 * Генерируем значения для публичного и приватного ключей, пишем их в файл. Длина ключа - keylength
	 * @param string 
	 */
	protected static Key createKeys(int keylength, String user) {
		// Подготовка к генерации
		Key result = new Key();
		SecureRandom r = new SecureRandom();

		BigInteger p = new BigInteger(keylength / 2, r);
		// Четное число не может быть простым
		if (p.remainder(new BigInteger("2")).equals(BigInteger.ZERO)){
			p = p.add(BigInteger.ONE);
		}
		// Проверяем простое ли число
		while (!BigIntegerHelper.isPrime(p, 50)){
			p = p.add(new BigInteger("2"));
		}
		BigInteger q = new BigInteger(keylength / 2, r);
		if (q.remainder(new BigInteger("2")).equals(BigInteger.ZERO)){
			q = q.add(BigInteger.ONE);
		}
		while (!BigIntegerHelper.isPrime(q, 50)){
			q = q.add(new BigInteger("2"));
		}
		BigInteger n = p.multiply(q);
		BigInteger m = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
		BigInteger e = new BigInteger("3");
		while (m.gcd(e).intValue() > 1) {
			e = e.add(new BigInteger("2"));
		}
		BigInteger d = e.modInverse(m);

		result.setCommonPart(n);
		result.setPrivatePart(e);
		result.setPublicPart(d);
		result.setUser(user);

		Log.i("RsaHelper", "key =  \n" + result.toString());
		return result;
	}

}
