package ru.veloc1.rsachat.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import ru.veloc1.rsachat.rsa.RSA.Key;
import ru.veloc1.rsachat.xmpp.data.Message;
import ru.veloc1.rsachat.xmpp.data.User;
import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

public enum DatabaseManager {

	INSTANCE;

	private static final String TAG = "DATABASE_MANAGER";

	private InnerDatabaseHelper innerHelperDB;

	public void init(Context context) {
		innerHelperDB = new InnerDatabaseHelper(context);
	}

	public void disconnect() {
		if (innerHelperDB.isOpen()) {
			innerHelperDB.close();
		}
	}

	// // ФУНКЦИИ РАБОТЫ С БД

	public <T> void clearTable(Class<T> clazz) {
		try {
			TableUtils.clearTable(innerHelperDB.getConnectionSource(), clazz);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public <T> Dao<T, ?> getDao(Class<T> clazz) {
		try {
			return innerHelperDB.getDao(clazz);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// // ФУНКЦИИ ДОБАВЛЕНИЯ

	public <T> void add(final T data, Class<T> clazz) {
		if (data == null)
			return;
		try {
			final Dao<T, Integer> dao = innerHelperDB.getDao(clazz);

			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					dao.create(data);
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG,
					String.format("Insert of %s WAS FAILED cause %s",
							clazz.getName(), e.toString()));
		}
	}

	public <T> void addList(final List<T> data, Class<T> clazz) {
		if (data.size() == 0)
			return;
		try {
			final Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					for (T item : data) {
						dao.create(item);
					}
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG,
					String.format("Insert of %s WAS FAILED cause %s",
							clazz.getName(), e.toString()));
		}
	}

	public <T> void delete(final T data, Class<T> clazz) {
		if (data == null)
			return;
		try {
			final Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					dao.delete(data);
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG,
					String.format("Delete of %s WAS FAILED cause %s",
							clazz.getName(), e.toString()));
		}
	}

	public <T> void deleteList(final List<T> data, Class<T> clazz) {
		if (data.size() == 0)
			return;
		try {
			final Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					dao.delete(data);
					// for (T item: data){
					// dao.delete(item);
					// }
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG,
					String.format("Delete of %s WAS FAILED cause %s",
							clazz.getName(), e.toString()));
		}
	}

	public <T> void deleteByField(final String columnName, final Object value,
			Class<T> clazz) {
		if (columnName == null)
			return;
		try {
			final Dao<T, String> dao = innerHelperDB.getDao(clazz);
			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					DeleteBuilder<T, String> deleteBuilder = dao
							.deleteBuilder();
					deleteBuilder.where().eq(columnName, value);
					deleteBuilder.delete();
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG,
					String.format("Delete of %s WAS FAILED cause %s",
							clazz.getName(), e.toString()));
		}
	}

	public <T> void createOrUpdate(final T data, Class<T> clazz) {
		if (data == null)
			return;

		try {
			final Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					dao.createOrUpdate(data);
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG, String.format("Insert/update of %s WAS FAILED cause %s",
					clazz.getName(), e.toString()));
		}
	}

	public <T> void createOrUpdate(final List<T> data, Class<T> clazz) {
		if (data.size() == 0)
			return;

		try {
			final Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			dao.callBatchTasks(new Callable<Void>() {

				public Void call() throws Exception {
					for (T item : data) {
						dao.createOrUpdate(item);
					}
					return null;
				}
			});
		} catch (Exception e) {
			Log.d(TAG, String.format("Insert/update of %s WAS FAILED cause %s",
					clazz.getName(), e.toString()));
		}
	}

	// // ФУНКЦИИ ИЗВЛЕЧЕНИЯ

	public <T> long getCount(Class<T> clazz) {
		try {
			Dao<T, String> dao = innerHelperDB.getDao(clazz);
			return dao.countOf();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public <T> T getSingleObject(Integer id, Class<T> clazz) {
		try {
			Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			return dao.queryForId(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public <T> ArrayList<T> getList(Class<T> clazz) {
		List<T> result;
		try {
			Dao<T, Integer> dao = innerHelperDB.getDao(clazz);
			result = dao.queryForAll();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return (ArrayList<T>) result;
	}

	public ArrayList<Message> getHistoryByUser(String user) {
		List<Message> result;
		try {
			Dao<Message, Integer> dao = innerHelperDB.getDao(Message.class);
			QueryBuilder<Message, Integer> builder = dao.queryBuilder();
			builder.where().like(Message.DB_TO, user).or()
					.like(Message.DB_FROM, user);
			result = builder.query();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return (ArrayList<Message>) result;
	}

	public User findUser(String user) {
		try {
			Dao<User, Integer> dao = innerHelperDB.getDao(User.class);
			QueryBuilder<User, Integer> builder = dao.queryBuilder();
			builder.where().eq(User.DB_USER, user);
			return builder.queryForFirst();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Key getKeyByUser(String user) {
		try {
			Dao<Key, Integer> dao = innerHelperDB.getDao(Key.class);
			QueryBuilder<Key, Integer> builder = dao.queryBuilder();
			builder.where().eq(Key.DB_USER, user);
			return builder.queryForFirst();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}