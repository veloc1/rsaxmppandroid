package ru.veloc1.rsachat.adapters;

import java.util.ArrayList;

import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.xmpp.data.User;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;


public class UserAdapter extends BaseAdapter {

	private ArrayList<User>	mItems;
	private LayoutInflater	mInflater;

	public UserAdapter(Context context) {
		mItems = new ArrayList<User>();
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public User getItem(int position) {
		return mItems.get(position);
	}

	@Override
	public boolean isEmpty() {
		return getCount() == 0 ? true : false;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView != null) {
			holder = (ViewHolder) convertView.getTag();
		} else {
			convertView = mInflater.inflate(R.layout.user_item, null, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		}

		holder.name.setText(getItem(position).getName());
		holder.status.setText(getItem(position).getStatus());
		holder.status.setBackgroundColor(getItem(position).getColor());
		if (getItem(position).isOffline()) {
			if (!getItem(position).isAuthorized()) {
				convertView.setBackgroundColor(0x40600000);
				holder.status.setText(R.string.not_authorized);
			} else {
				convertView.setBackgroundColor(0x30000000);
			}
		} else {
			convertView.setBackgroundColor(0x00000000);
		}
		return convertView;
	}

	public void setItems(ArrayList<User> items) {
		mItems = items;
		notifyDataSetChanged();
	}

	protected static class ViewHolder {

		@InjectView(R.id.user_item_name)
		TextView	name;
		@InjectView(R.id.user_item_status)
		TextView	status;

		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
		}
	}
}
