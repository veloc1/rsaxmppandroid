package ru.veloc1.rsachat.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import ru.veloc1.rsachat.ChatApplication;
import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.xmpp.data.Message;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class MessageAdapter extends BaseAdapter {

	private final static int SYSTEM = 0;
	private final static int IN = 1;
	private final static int OUT = 2;

	private ArrayList<Message> mItems;
	private LayoutInflater mInflater;
	private SimpleDateFormat mPrevFormat;
	private Calendar mCalendar;
	private SimpleDateFormat mTodayFormat;

	public MessageAdapter(Context context) {
		mItems = new ArrayList<Message>();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mPrevFormat = new SimpleDateFormat("yyy-MM-dd HH:mm",
				Locale.getDefault());
		mTodayFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
		mCalendar = Calendar.getInstance();
	}

	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public boolean isEmpty() {
		return getCount() == 0 ? true : false;
	}

	@Override
	public Message getItem(int position) {
		return mItems.get(position);
	}

	@Override
	public int getViewTypeCount() {
		return 3;
	}

	@Override
	public int getItemViewType(int position) {
		Message msg = getItem(position);
		if (msg.isSystem())
			return SYSTEM;
		else {
			String self = ChatApplication.getSelfUser();
			if (msg.getFrom().equals(self)) {
				return OUT;
			} else {
				return IN;
			}
		}
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		switch (getItemViewType(position)) {
		case SYSTEM:
			convertView = mInflater.inflate(R.layout.message_item_sys, parent,
					false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
			break;

		case IN:
			convertView = mInflater.inflate(R.layout.message_item_in, parent,
					false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
			break;

		case OUT:
			convertView = mInflater.inflate(R.layout.message_item_out, parent,
					false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
			break;

		default:
			break;
		}

		holder.body.setText(getItem(position).getMessage());
		long delta = System.currentTimeMillis()
				- getItem(position).getTimestamp();
		mCalendar.setTimeInMillis(getItem(position).getTimestamp());
		if (delta > 1000 * 60 * 60 * 24) {
			holder.time.setText(mPrevFormat.format(mCalendar.getTime()));
		} else {
			holder.time.setText(mTodayFormat.format(mCalendar.getTime()));
		}

		return convertView;
	}

	public void addItem(Message message) {
		mItems.add(message);
		notifyDataSetChanged();
	}

	public void setItems(ArrayList<Message> items) {
		mItems = items;
		notifyDataSetChanged();
	}

	protected static class ViewHolder {

		@InjectView(R.id.message_item_body)
		TextView body;
		@InjectView(R.id.message_item_timestamp)
		TextView time;

		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
		}
	}
}
