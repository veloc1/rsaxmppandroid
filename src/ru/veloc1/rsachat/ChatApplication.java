package ru.veloc1.rsachat;

import org.jivesoftware.smack.SmackAndroid;

import ru.veloc1.rsachat.db.DatabaseManager;
import ru.veloc1.rsachat.xmpp.SimpleXmppManager;
import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.lowlevel.InnerCode;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;


public class ChatApplication extends Application {

	public final static String			PREFERENCES_NAME	= "preferences";
	private static String				SELF_USER;

	private static SharedPreferences	mPreferences;
	private static String	mCurrentChat;

	@Override
	public void onCreate() {
		super.onCreate();
		SmackAndroid.init(this);
		mPreferences = getApplicationContext().getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
		DatabaseManager.INSTANCE.init(getApplicationContext());
	}

	@Override
	public void onTerminate() {
		Intent serviceIntent = new Intent(this, XmppService.class);
		serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.STOP_CODE);
		startService(serviceIntent);
		DatabaseManager.INSTANCE.disconnect();
		super.onTerminate();
	}

	public static String getSelfUser() {
		SELF_USER = mPreferences.getString(XmppService.USER, "");
		return SELF_USER.concat("@".concat(SimpleXmppManager.SERVICE));
	}

	public static void setCurrentChat(String userJID) {
		mCurrentChat = userJID;
	}

	public static void unsetCurrentChat() {
		mCurrentChat = null;
	}
	
	public static String getCurrentChat(){
		return mCurrentChat;
	}

}
