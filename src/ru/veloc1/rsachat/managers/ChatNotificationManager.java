package ru.veloc1.rsachat.managers;

import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.activities.MessengerActivity;
import ru.veloc1.rsachat.xmpp.XmppService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;


public enum ChatNotificationManager {

	INSTANCE;

	public void addNotification(Context context, int id, String user, String messange) {
		Intent messIntent = new Intent(context, MessengerActivity.class);
		messIntent.putExtra(XmppService.USER, user);
		messIntent.setAction(String.valueOf(System.currentTimeMillis()));
		PendingIntent p = PendingIntent.getActivity(context, 0, messIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder b = new Builder(context);
		b.setContentText(messange);
		b.setContentTitle(user);
		b.setContentIntent(p);
		b.setAutoCancel(true);
		// b.setSound();
		b.setSmallIcon(R.drawable.ic_launcher);
		manager.notify(id, b.build());
	}
}
