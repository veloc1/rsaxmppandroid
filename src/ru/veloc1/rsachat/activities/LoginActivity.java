package ru.veloc1.rsachat.activities;

import ru.veloc1.rsachat.ChatApplication;
import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.lowlevel.InnerCode;
import ru.veloc1.rsachat.xmpp.receivers.OnlineStatusReceiver;
import ru.veloc1.rsachat.xmpp.receivers.OnlineStatusReceiver.OnlineStatusRunnable;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Activity which displays a login screen to the user, offering registration as well.
 */
public class LoginActivity extends Activity {

	/**
	 * The default email to populate the email field with.
	 */
	public static final String		EXTRA_EMAIL	= "extra.EMAIL";

	// Values for email and password at the time of the login attempt.
	private String					mEmail;
	private String					mPassword;

	// UI references.
	private EditText				mEmailView;
	private EditText				mPasswordView;
	private View					mLoginFormView;
	private View					mLoginStatusView;
	private TextView				mLoginStatusMessageView;

	private OnlineStatusReceiver	mOnlineReceiver;

	private SharedPreferences		mPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		mPreferences = getApplicationContext().getSharedPreferences(ChatApplication.PREFERENCES_NAME, MODE_PRIVATE);
		if (getIntent() != null) {
			if (getIntent().getBooleanExtra(XmppService.EXIT, false)) {
				Editor e = mPreferences.edit();
				e.putString(XmppService.USER, "");
				e.putString(XmppService.PASSWORD, "");
				e.commit();
			}
		}

		// Set up the login form.
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					attemptLogin();
					return true;
				}
				return false;
			}
		});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});

		startLoginService(true);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mOnlineReceiver = new OnlineStatusReceiver(new OnlineStatusRunnable() {

			@Override
			public void run() {
				if (mStatus == 1) {
					Intent intent = new Intent(LoginActivity.this, ChatListActivity.class);
					startActivity(intent);
					finish();
					unregisterReceiver(mOnlineReceiver);
				} else {
					mEmailView.setText(mPreferences.getString(XmppService.USER, ""));
					mPasswordView.setText(mPreferences.getString(XmppService.PASSWORD, ""));
					showProgress(false);
					mPasswordView.setError(getString(R.string.error_incorrect_password));
					mPasswordView.requestFocus();
				}
			}
		});
		registerReceiver(mOnlineReceiver, new IntentFilter(XmppService.ACTION_ONLINE));
	}

	@Override
	protected void onPause() {
		try {
			unregisterReceiver(mOnlineReceiver);
		}
		catch (Exception e) {
			// hacky hack
		}
		super.onPause();
	}

	/**
	 * Attempts to sign in or register the account specified by the login form. If there are form errors (invalid email, missing fields,
	 * etc.), the errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}
		// else if (!mEmail.contains("@")) {
		// mEmailView.setError(getString(R.string.error_invalid_email));
		// focusView = mEmailView;
		// cancel = true;
		// }

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			startLoginService(false);
		}
	}

	private void startLoginService(boolean onStart) {
		Intent serviceIntent = new Intent(this, XmppService.class);
		if (!onStart) {
			serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.START_CODE);
			serviceIntent.putExtra(XmppService.USER, mEmail);
			serviceIntent.putExtra(XmppService.PASSWORD, mPassword);
		} else {
			if (getIntent() != null) {
				if (!getIntent().getBooleanExtra(XmppService.EXIT, false)) {
					serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.RESTART_CODE);
				}
			} else {
				serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.RESTART_CODE);
			}
		}
		startService(serviceIntent);
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {

				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {

				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
}
