package ru.veloc1.rsachat.activities;

import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.lowlevel.InnerCode;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;


public class SettingsActivity extends PreferenceActivity {

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		setupSimplePreferencesScreen();
	}

	@SuppressWarnings("deprecation")
	private void setupSimplePreferencesScreen() {
		addPreferencesFromResource(R.xml.pref_empty);

		Preference exit = new Preference(this);
		exit.setTitle(R.string.exit);
		exit.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder b = new AlertDialog.Builder(SettingsActivity.this);
				b.setTitle(R.string.exit);
				b.setMessage(R.string.sure_exit);
				b.setPositiveButton(android.R.string.yes, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent serviceIntent = new Intent(SettingsActivity.this, XmppService.class);
						serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.STOP_CODE);
						startService(serviceIntent);
						setResult(RESULT_OK);
						finish();
					}
				});
				b.setNegativeButton(android.R.string.no, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				b.show();
				return true;
			}
		});
		getPreferenceScreen().addPreference(exit);
	}
}
