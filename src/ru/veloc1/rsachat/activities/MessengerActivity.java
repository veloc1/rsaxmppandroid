package ru.veloc1.rsachat.activities;

import java.util.ArrayList;

import ru.veloc1.rsachat.ChatApplication;
import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.adapters.MessageAdapter;
import ru.veloc1.rsachat.db.DatabaseManager;
import ru.veloc1.rsachat.rsa.RSA;
import ru.veloc1.rsachat.rsa.lowlevel.KeyStorage;
import ru.veloc1.rsachat.xmpp.SimpleXmppManager;
import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.data.Message;
import ru.veloc1.rsachat.xmpp.lowlevel.InnerCode;
import ru.veloc1.rsachat.xmpp.receivers.MessageBroadcastReceiver;
import ru.veloc1.rsachat.xmpp.receivers.MessageBroadcastReceiver.MessageBroadcastRunnable;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;


public class MessengerActivity extends Activity {

	@InjectView(R.id.messenger_edit_text)
	protected EditText					mEditText;
	@InjectView(R.id.messenger_list)
	protected ListView					mMessageList;
	@InjectView(R.id.messenger_send)
	protected Button					mSendButton;
	private String						mUser;
	private MessageBroadcastReceiver	mMessageBroadcastReceiver;
	private MessageAdapter				mAdapter;
	private boolean						mIsRsaEnabled;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messenger);
		ButterKnife.inject(this);

		mUser = getIntent().getStringExtra(XmppService.USER);
		setTitle(mUser);

		setListeners();

		mAdapter = new MessageAdapter(this);
		mMessageList.setAdapter(mAdapter);

		ArrayList<Message> history = DatabaseManager.INSTANCE.getHistoryByUser(mUser);
		if (history != null && history.size() > 0) {
			mAdapter.setItems(history);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.messenger, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_enable_rsa:
				if (KeyStorage.INSTANCE.restoreKey(ChatApplication.getSelfUser()) == null) {
					Toast.makeText(this, R.string.need_to_gen_keys, Toast.LENGTH_SHORT).show();
					break;
				}
				mIsRsaEnabled = !mIsRsaEnabled;
				if (mIsRsaEnabled) {
					Intent chatService = new Intent(MessengerActivity.this, XmppService.class);
					chatService.putExtra(XmppService.COMMAND_CODE, InnerCode.SEND_KEY);
					chatService.putExtra(XmppService.USER, mUser);
					chatService.putExtra(
							XmppService.MESSAGE,
							KeyStorage.INSTANCE.restoreKey(ChatApplication.getSelfUser()).getPublicPart().toString().concat(" ")
									.concat(KeyStorage.INSTANCE.restoreKey(ChatApplication.getSelfUser()).getCommonPart().toString()));
					startService(chatService);
				}
				break;

			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		startServices();
		registerReceivers();
		ChatApplication.setCurrentChat(mUser);
	}

	@Override
	protected void onPause() {
		stopServices();
		unregisterReceivers();
		ChatApplication.unsetCurrentChat();
		super.onPause();
	}

	private void registerReceivers() {
		mMessageBroadcastReceiver = new MessageBroadcastReceiver(new MessageBroadcastRunnable() {

			@Override
			public void run() {
				String body = message.getMessage();
				if (mIsRsaEnabled) {
					if (!message.isSystem()) {
						message.setMessage(RSA.INSTANCE.decypher(body));
					}
				}
				mAdapter.addItem(message);
			}
		});
		registerReceiver(mMessageBroadcastReceiver, new IntentFilter(XmppService.ACTION_MESSAGE));
	}

	private void startServices() {}

	private void unregisterReceivers() {
		unregisterReceiver(mMessageBroadcastReceiver);
	}

	private void stopServices() {}

	private void setListeners() {
		mSendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String message = mEditText.getText().toString();
				if (message.trim().length() > 0) {
					Intent chatService = new Intent(MessengerActivity.this, XmppService.class);
					chatService.putExtra(XmppService.COMMAND_CODE, InnerCode.SEND_MESSAGE);
					chatService.putExtra(XmppService.USER, mUser);
					if (mIsRsaEnabled) {
						ru.veloc1.rsachat.xmpp.data.Message recvMsg = new ru.veloc1.rsachat.xmpp.data.Message(SimpleXmppManager.INSTANCE
								.getSelfUser(), mUser, message, System.currentTimeMillis(), false);
						DatabaseManager.INSTANCE.add(recvMsg, ru.veloc1.rsachat.xmpp.data.Message.class);
						mAdapter.addItem(recvMsg);
						message = RSA.INSTANCE.cypher(message, mUser);
					}
					chatService.putExtra(XmppService.MESSAGE, message);
					startService(chatService);
				}
				mEditText.setText("");
			}
		});
	}

}
