package ru.veloc1.rsachat.activities;

import ru.veloc1.rsachat.ChatApplication;
import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.adapters.UserAdapter;
import ru.veloc1.rsachat.rsa.RSA;
import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.lowlevel.InnerCode;
import ru.veloc1.rsachat.xmpp.receivers.AddedUserStatusReceiver;
import ru.veloc1.rsachat.xmpp.receivers.OnlineStatusReceiver;
import ru.veloc1.rsachat.xmpp.receivers.UsersReceiver;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;


public class ChatListActivity extends Activity {

	public final static int			CODE_EXIT	= 44;

	@InjectView(R.id.chat_list_users)
	protected ListView				mUsersList;
	@InjectView(R.id.chat_list_status)
	protected TextView				mStatusText;
	@InjectView(R.id.chat_list_add)
	protected TextView				mAddText;

	private UserAdapter				mAdapter;
	private OnlineStatusReceiver	mOnlineReceiver;
	private UsersReceiver			mUsersReceiver;
	private AddedUserStatusReceiver	mAddedReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat_list);
		ButterKnife.inject(this);

		mAdapter = new UserAdapter(this);
		mUsersList.setAdapter(mAdapter);
		setListeners();
	}

	@Override
	protected void onResume() {
		super.onResume();

		startXmppService();
		registerReceivers();
	}

	@Override
	protected void onPause() {
		unregisterReceivers();
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.chat_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				Intent settingsIntent = new Intent(this, SettingsActivity.class);
				startActivityForResult(settingsIntent, CODE_EXIT);
				break;

			case R.id.action_generate_keys:
				RSA.INSTANCE.generateAndSaveKeys();
				break;

			case R.id.action_test_rsa:
				AlertDialog.Builder builder = new Builder(this);
				View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.test_rsa_layout,
						null, false);
				final EditText testEdit = (EditText) dialogView.findViewById(R.id.test_rsa_edit_text);
				final TextView testText = (TextView) dialogView.findViewById(R.id.test_rsa_text);
				builder.setView(dialogView);
				builder.setNeutralButton(R.string.test, null);
				final AlertDialog adialog = builder.create();
				adialog.setOnShowListener(new OnShowListener() {

					@Override
					public void onShow(DialogInterface dialog) {
						adialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								String mess = testEdit.getText().toString();
								testText.setText(RSA.INSTANCE.cypher(mess, ChatApplication.getSelfUser()));
							}
						});
					}
				});
				adialog.show();
				break;

			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case CODE_EXIT:
				if (resultCode == Activity.RESULT_OK) {
					Intent loginIntent = new Intent(this, LoginActivity.class);
					loginIntent.putExtra(XmppService.EXIT, true);
					startActivity(loginIntent);
					finish();
				}
				break;

			default:
				break;
		}
	}

	private void startXmppService() {
		Intent serviceIntent = new Intent(this, XmppService.class);
		serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.GET_USERS_CODE);
		startService(serviceIntent);
		Intent nserviceIntent = new Intent(this, XmppService.class);
		nserviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.IS_ONLINE_CODE);
		startService(nserviceIntent);
	}

	private void registerReceivers() {
		mOnlineReceiver = new OnlineStatusReceiver(mStatusText);
		registerReceiver(mOnlineReceiver, new IntentFilter(XmppService.ACTION_ONLINE));

		mUsersReceiver = new UsersReceiver(mAdapter);
		registerReceiver(mUsersReceiver, new IntentFilter(XmppService.ACTION_USERS));

		mAddedReceiver = new AddedUserStatusReceiver(this);
		registerReceiver(mAddedReceiver, new IntentFilter(XmppService.ACTION_ADD_USER));
	}

	private void unregisterReceivers() {
		unregisterReceiver(mOnlineReceiver);
		unregisterReceiver(mUsersReceiver);
		unregisterReceiver(mAddedReceiver);
	}

	private void openChatActivity(String jid) {
		Intent messIntent = new Intent(ChatListActivity.this, MessengerActivity.class);
		messIntent.putExtra(XmppService.USER, jid);
		startActivity(messIntent);
	}

	private void addUserToRoster(String jid) {
		Intent serviceIntent = new Intent(this, XmppService.class);
		serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.ADD_USER_CODE);
		serviceIntent.putExtra(XmppService.USER, jid);
		startService(serviceIntent);
	}

	private void removeFromRoster(String jid) {
		Intent serviceIntent = new Intent(this, XmppService.class);
		serviceIntent.putExtra(XmppService.COMMAND_CODE, InnerCode.REMOVE_USER_CODE);
		serviceIntent.putExtra(XmppService.USER, jid);
		startService(serviceIntent);
	}

	private void setListeners() {
		mAddText.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder b = new AlertDialog.Builder(ChatListActivity.this);
				b.setTitle(R.string.add_text);
				final EditText et = new EditText(ChatListActivity.this);
				b.setView(et);
				b.setNegativeButton(android.R.string.cancel, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton(android.R.string.ok, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						addUserToRoster(et.getText().toString());
					}
				});
				b.show();
			}
		});
		mUsersList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				openChatActivity(mAdapter.getItem(arg2).getUser());
			}
		});

		mUsersList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
				AlertDialog.Builder b = new Builder(ChatListActivity.this);
				b.setItems(R.array.contact_menu, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case 0:
								openChatActivity(mAdapter.getItem(arg2).getUser());
								break;
							case 1:
								removeFromRoster(mAdapter.getItem(arg2).getUser());
								break;

							default:
								break;
						}
					}
				});
				b.show();
				return true;
			}
		});
	}
}
