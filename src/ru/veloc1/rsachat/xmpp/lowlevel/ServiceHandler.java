package ru.veloc1.rsachat.xmpp.lowlevel;

import java.math.BigInteger;
import java.util.Collection;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;

import ru.veloc1.rsachat.rsa.RSA.Key;
import ru.veloc1.rsachat.rsa.lowlevel.KeyStorage;
import ru.veloc1.rsachat.xmpp.SimpleXmppManager;
import ru.veloc1.rsachat.xmpp.XmppService;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class ServiceHandler extends Handler {

	private XmppService mService;

	public ServiceHandler(Looper looper, XmppService xmppService) {
		super(looper);
		mService = xmppService;
	}

	@Override
	public void handleMessage(Message msg) {
		synchronized (this) {
			Bundle dataBundle = msg.getData();
			if (dataBundle != null) {
				InnerCode code = (InnerCode) dataBundle
						.getSerializable(XmppService.COMMAND_CODE);
				if (code != null) {
					switch (code) {
					case START_CODE:
						String user = dataBundle.getString(XmppService.USER);
						String password = dataBundle
								.getString(XmppService.PASSWORD);
						int connected = SimpleXmppManager.INSTANCE.connect(
								user, password, mService);
						mService.setConnected(connected);
						setRosterListener();
						setChatListener();
						break;
					case RESTART_CODE:
						String ruser = dataBundle.getString(XmppService.USER);
						String rpassword = dataBundle
								.getString(XmppService.PASSWORD);
						if (ruser.length() > 0 && rpassword.length() > 0) {
							int rconnected = SimpleXmppManager.INSTANCE
									.connect(ruser, rpassword, mService);
							mService.setConnected(rconnected);
							setRosterListener();
							setChatListener();
						}
						break;

					case STOP_CODE:
						SimpleXmppManager.INSTANCE.disconnect();
						break;

					case GET_ROSTER_CODE:
						mService.setRoster(SimpleXmppManager.INSTANCE
								.getRoster());
						break;

					case GET_USERS_CODE:
						mService.setUsers(SimpleXmppManager.INSTANCE.getUsers());
						break;

					case ADD_USER_CODE:
						boolean added = SimpleXmppManager.INSTANCE
								.addUser(dataBundle.getString(XmppService.USER));
						mService.notifyAddUser(added);
						break;

					case REMOVE_USER_CODE:
						SimpleXmppManager.INSTANCE
								.removeUserFromRoster(dataBundle
										.getString(XmppService.USER));
						break;

					case IS_ONLINE_CODE:
						if (SimpleXmppManager.INSTANCE.isConnected()) {
							mService.setConnected(1);
						} else {
							mService.setConnected(0);
						}
						break;

					case SEND_MESSAGE:
						boolean sended = SimpleXmppManager.INSTANCE
								.sendMessage(
										dataBundle.getString(XmppService.USER),
										dataBundle
												.getString(XmppService.MESSAGE));
						if (sended) {
							ru.veloc1.rsachat.xmpp.data.Message sendedMsg = new ru.veloc1.rsachat.xmpp.data.Message(
									SimpleXmppManager.INSTANCE.getSelfUser(),
									dataBundle.getString(XmppService.USER),
									dataBundle.getString(XmppService.MESSAGE),
									System.currentTimeMillis(), false);
							mService.message(sendedMsg);
						}
						break;

					case MESSAGE_RECEIVED:

						break;

					case SEND_KEY:
						boolean ksended = SimpleXmppManager.INSTANCE.sendKey(
								dataBundle.getString(XmppService.USER),
								dataBundle.getString(XmppService.MESSAGE));
						if (ksended) {
							ru.veloc1.rsachat.xmpp.data.Message sendedMsg = new ru.veloc1.rsachat.xmpp.data.Message(
									SimpleXmppManager.INSTANCE.getSelfUser(),
									dataBundle.getString(XmppService.USER),
									dataBundle.getString(XmppService.MESSAGE),
									System.currentTimeMillis(), true);
							mService.message(sendedMsg);
						}
						break;

					default:
						break;
					}
				}
			}
		}
	}

	private void setRosterListener() {
		SimpleXmppManager.INSTANCE.getRoster().addRosterListener(
				new RosterListener() {

					@Override
					public void presenceChanged(Presence arg0) {
						mService.setUsers(SimpleXmppManager.INSTANCE.getUsers());
					}

					@Override
					public void entriesUpdated(Collection<String> arg0) {
						mService.setUsers(SimpleXmppManager.INSTANCE.getUsers());
					}

					@Override
					public void entriesDeleted(Collection<String> arg0) {
						mService.setUsers(SimpleXmppManager.INSTANCE.getUsers());
					}

					@Override
					public void entriesAdded(Collection<String> arg0) {
						mService.setUsers(SimpleXmppManager.INSTANCE.getUsers());
					}
				});
	}

	private void setChatListener() {
		ChatManager m = SimpleXmppManager.INSTANCE.getChatManager();
		if (m != null) {
			m.addChatListener(new ChatManagerListener() {

				@Override
				public void chatCreated(Chat chat, boolean createdLocally) {
					if (!createdLocally) {
						chat.addMessageListener(new ChatMessageListener(
								mService));
					}
				}
			});
		}
	}

	public static class ChatMessageListener implements MessageListener {

		private XmppService mService;

		public ChatMessageListener(XmppService service) {
			mService = service;
		}

		@Override
		public void processMessage(Chat chat,
				org.jivesoftware.smack.packet.Message message) {
			if (message.getBody(XmppService.MESSAGE) != null) {
				if (message.getBody(XmppService.MESSAGE)
						.equals(XmppService.KEY)) {
					if (message.getBody(XmppService.KEY) != null
							&& message.getBody(XmppService.KEY).length() > 0) {
						String from = chat.getParticipant();
						if (chat.getParticipant().indexOf("/") != -1) {
							from = from.substring(0, chat.getParticipant()
									.indexOf("/"));
						}
						ru.veloc1.rsachat.xmpp.data.Message recvMsg = new ru.veloc1.rsachat.xmpp.data.Message(
								from, SimpleXmppManager.INSTANCE.getSelfUser(),
								message.getBody(XmppService.KEY),
								System.currentTimeMillis(), true);
						Key userKey = new Key();
						userKey.setUser(from);
						String[] keys = message.getBody(XmppService.KEY).split(
								" ");
						userKey.setPublicPart(new BigInteger(keys[0]));
						userKey.setCommonPart(new BigInteger(keys[1]));
						KeyStorage.INSTANCE.saveKey(userKey);
						mService.message(recvMsg);
						return;
					}
				}
			}
			if (message.getBody() != null && message.getBody().length() > 0) {
				String from = chat.getParticipant();
				if (chat.getParticipant().indexOf("/") != -1) {
					from = from
							.substring(0, chat.getParticipant().indexOf("/"));
				}
				ru.veloc1.rsachat.xmpp.data.Message recvMsg = new ru.veloc1.rsachat.xmpp.data.Message(
						from, SimpleXmppManager.INSTANCE.getSelfUser(),
						message.getBody(),
						System.currentTimeMillis(), false);
				mService.message(recvMsg);
			}
		}
	};
}
