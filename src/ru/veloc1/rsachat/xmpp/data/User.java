package ru.veloc1.rsachat.xmpp.data;

import java.io.Serializable;

import org.jivesoftware.smack.packet.Presence;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "users")
public class User implements Parcelable, Serializable {

	private static final long						serialVersionUID	= -8064461774644280301L;
	public static final String						DB_USER				= "user";

	public static final Parcelable.Creator<User>	CREATOR				= new Creator<User>() {

																			@Override
																			public User createFromParcel(Parcel source) {
																				return new User(source);
																			}

																			@Override
																			public User[] newArray(int size) {
																				return new User[size];
																			}

																		};

	@DatabaseField(generatedId = true)
	private int										id;
	@DatabaseField
	private String									mName;
	@DatabaseField(columnName = DB_USER)
	private String									mUser;
	@DatabaseField
	private String									mStatus;
	@DatabaseField
	private int										mColor;
	@DatabaseField
	private boolean									mIsOnline;
	@DatabaseField
	private boolean									mIsAuthorized;

	public User() {}

	public User(String name, String user, Presence presence, boolean authorized) {
		setName(name);
		setUser(user);
		if (presence != null) {
			mIsOnline = true;
			setStatus(presence.getStatus());
			if (presence.getMode() != null) {
				switch (presence.getMode()) {
					case available:
						setColor(0x8000ff00);
						break;

					case away:
						setColor(0x80ffff00);
						break;

					case dnd:
						setColor(0x80ff0000);
						break;

					default:
						setColor(0x00000000);
						break;
				}
			} else {
				if (presence.getType().equals(Presence.Type.available)) {
					setColor(0x8000ff00);
				} else {
					setColor(0x00000000);
				}
			}
		} else {
			setStatus("");
			setColor(0x00000000);
		}

		mIsAuthorized = authorized;

		if (presence.getType().equals(Presence.Type.unavailable)) {
			mIsOnline = false;
		}
	}

	public User(Parcel source) {
		setName(source.readString());
		setUser(source.readString());
		setStatus(source.readString());
		setColor(source.readInt());
		if (source.readByte() == 1) {
			mIsOnline = true;
		} else {
			mIsOnline = false;
		}
		if (source.readByte() == 1) {
			mIsAuthorized = true;
		} else {
			mIsAuthorized = false;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public String getStatus() {
		return mStatus;
	}

	public void setStatus(String status) {
		mStatus = status;
	}

	@Override
	public int describeContents() {
		return hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mName);
		dest.writeString(mUser);
		dest.writeString(mStatus);
		dest.writeInt(mColor);
		byte b = 0;
		if (mIsOnline) {
			b = 1;
			dest.writeByte(b);
		} else {
			dest.writeByte(b);
		}
		b = 0;
		if (mIsAuthorized) {
			b = 1;
			dest.writeByte(b);
		} else {
			dest.writeByte(b);
		}
	}

	public int getColor() {
		return mColor;
	}

	public void setColor(int color) {
		mColor = color;
	}

	public String getUser() {
		return mUser;
	}

	public void setUser(String user) {
		mUser = user;
	}

	public boolean isOffline() {
		return !mIsOnline;
	}

	public boolean isAuthorized() {
		return mIsAuthorized;
	}
}
