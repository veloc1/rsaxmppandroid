package ru.veloc1.rsachat.xmpp.data;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "messages")
public class Message implements Parcelable, Serializable {

	private static final long						serialVersionUID	= -8011499854464614004L;

	public static final String						DB_TO				= "to";
	public static final String						DB_FROM				= "from";

	public static final Parcelable.Creator<Message>	CREATOR				= new Creator<Message>() {

																			@Override
																			public Message createFromParcel(Parcel source) {
																				return new Message(source);
																			}

																			@Override
																			public Message[] newArray(int size) {
																				return new Message[size];
																			}

																		};

	@DatabaseField(generatedId = true)
	private int										id;
	@DatabaseField(columnName = DB_TO)
	private String									to;
	@DatabaseField(columnName = DB_FROM)
	private String									from;
	@DatabaseField
	private String									message;
	@DatabaseField
	private long									timestamp;
	@DatabaseField
	private boolean									isSystem;

	public Message() {}

	public Message(String from, String to, String message, long timestamp, boolean isSystem) {
		this.from = from;
		this.to = to;
		this.message = message;
		this.timestamp = timestamp;
		this.isSystem = isSystem;
	}

	public Message(Parcel source) {
		setFrom(source.readString());
		setTo(source.readString());
		setMessage(source.readString());
		setTimestamp(source.readLong());
		if (source.readByte() == 1) {
			isSystem = true;
		} else {
			isSystem = false;
		}
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isSystem() {
		return isSystem;
	}

	public void setSystem(boolean isSystem) {
		this.isSystem = isSystem;
	}

	@Override
	public int describeContents() {
		return hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(from);
		dest.writeString(to);
		dest.writeString(message);
		dest.writeLong(timestamp);
		byte b = 0;
		if (isSystem) {
			b = 1;
		}
		dest.writeByte(b);
	}
}
