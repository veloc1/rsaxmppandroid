package ru.veloc1.rsachat.xmpp;

import java.util.ArrayList;

import org.jivesoftware.smack.Roster;

import ru.veloc1.rsachat.ChatApplication;
import ru.veloc1.rsachat.db.DatabaseManager;
import ru.veloc1.rsachat.managers.ChatNotificationManager;
import ru.veloc1.rsachat.xmpp.data.User;
import ru.veloc1.rsachat.xmpp.lowlevel.InnerCode;
import ru.veloc1.rsachat.xmpp.lowlevel.ServiceHandler;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Binder;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.Process;

public class XmppService extends Service {

	public final static String COMMAND_CODE = "command_code";

	public final static String ACTION_ONLINE = "action_online";
	public final static String ACTION_ROSTER = "action_roster";
	public final static String ACTION_USERS = "action_users";
	public final static String ACTION_ADD_USER = "action_add_user";
	public final static String ACTION_MESSAGE = "action_message";

	public final static String STATUS_ONLINE = "status_online";
	public final static String ROSTER = "roster";
	public final static String USERS = "users";
	public final static String USER = "user_info";
	public final static String CHAT = "chat";
	public final static String MESSAGE = "message";
	public final static String KEY = "key";
	public final static String PASSWORD = "password";
	public final static String ADDED = "added";
	public final static String TO = "to";
	public final static String FROM = "from";
	public final static String EXIT = "exit";

	private final static String THREAD_NAME = "XmppThread";
	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;

	private ServiceBinder mBinder;
	private SharedPreferences mPreferences;

	@Override
	public void onCreate() {
		super.onCreate();
		mBinder = new ServiceBinder();
		HandlerThread thread = new HandlerThread(THREAD_NAME,
				Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper, this);

		mPreferences = getApplicationContext().getSharedPreferences(
				ChatApplication.PREFERENCES_NAME, MODE_PRIVATE);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			Message msg = mServiceHandler.obtainMessage();

			Bundle bundleToSend = new Bundle();
			InnerCode code = (InnerCode) intent
					.getSerializableExtra(COMMAND_CODE);
			if (code != null) {
				bundleToSend.putSerializable(COMMAND_CODE, code);
				switch (code) {
				case START_CODE:
					Editor e = mPreferences.edit();
					e.putString(USER, intent.getStringExtra(USER));
					e.putString(PASSWORD, intent.getStringExtra(PASSWORD));
					e.commit();

					bundleToSend.putString(USER, intent.getStringExtra(USER));
					bundleToSend.putString(PASSWORD,
							intent.getStringExtra(PASSWORD));
					break;

				case RESTART_CODE:
					bundleToSend.putString(USER,
							mPreferences.getString(USER, ""));
					bundleToSend.putString(PASSWORD,
							mPreferences.getString(PASSWORD, ""));
					break;

				case STOP_CODE:
					Editor ce = mPreferences.edit();
					ce.putString(USER, "");
					ce.putString(PASSWORD, "");
					ce.commit();
					stopSelf();
					break;

				case GET_ROSTER_CODE:
					break;

				case GET_USERS_CODE:
					break;

				case ADD_USER_CODE:
					bundleToSend.putString(USER, intent.getStringExtra(USER));
					break;

				case REMOVE_USER_CODE:
					bundleToSend.putString(USER, intent.getStringExtra(USER));
					break;

				case IS_ONLINE_CODE:
					break;

				case SEND_MESSAGE:
					String user = intent.getStringExtra(USER);
					String message = intent.getStringExtra(MESSAGE);
					if (user != null && user.length() > 0) {
						bundleToSend.putString(USER, user);
						bundleToSend.putString(MESSAGE, message);
					}
					break;

				case MESSAGE_RECEIVED:
					break;

				case SEND_KEY:
					String kuser = intent.getStringExtra(USER);
					String kmessage = intent.getStringExtra(MESSAGE);
					if (kuser != null && kuser.length() > 0) {
						bundleToSend.putString(USER, kuser);
						bundleToSend.putString(MESSAGE, kmessage);
					}
					break;

				default:
					break;
				}
			}
			msg.setData(bundleToSend);
			mServiceHandler.sendMessage(msg);
		}

		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	@Override
	public void onDestroy() {
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = -1;
		mServiceHandler.sendMessage(msg);
		super.onDestroy();
	}

	public void setConnected(int c) {
		Intent onlineIntent = new Intent(ACTION_ONLINE);
		if (c == 1) {
			onlineIntent.putExtra(STATUS_ONLINE, 1); // online
		} else {
			onlineIntent.putExtra(STATUS_ONLINE, -1); // error
		}
		sendBroadcast(onlineIntent);
	}

	public void setRoster(Roster roster) {
		// Intent rosterIntent = new Intent(ACTION_ROSTER);
		// rosterIntent.putExtra(ROSTER, roster);
		// sendBroadcast(rosterIntent);
	}

	public void setUsers(ArrayList<User> users) {
		Intent usersIntent = new Intent(ACTION_USERS);
		usersIntent.putParcelableArrayListExtra(USERS, users);
		sendBroadcast(usersIntent);
		DatabaseManager.INSTANCE.createOrUpdate(users, User.class);
	}

	public void notifyAddUser(boolean added) {
		Intent usersIntent = new Intent(ACTION_ADD_USER);
		usersIntent.putExtra(ADDED, added);
		sendBroadcast(usersIntent);
	}

	public void message(ru.veloc1.rsachat.xmpp.data.Message msg) {
		Intent messIntent = new Intent(ACTION_MESSAGE);
		messIntent.putExtra(MESSAGE, (Parcelable) msg);
		sendBroadcast(messIntent);
		DatabaseManager.INSTANCE.add(msg,
				ru.veloc1.rsachat.xmpp.data.Message.class);
		String user;
		if (ChatApplication.getSelfUser().equals(msg.getFrom())) {
			user = msg.getTo();
		} else {
			user = msg.getFrom();
		}
		if (ChatApplication.getCurrentChat() == null
				|| !ChatApplication.getCurrentChat().equals(user)) {
			String current = ChatApplication.getCurrentChat();
			boolean notify = true;

			if (current != null) {
				if (current.equals(msg.getFrom())) {
					notify = false;
				} else if (current.equals(msg.getTo())) {
					notify = false;
				}
			}

			User u = DatabaseManager.INSTANCE.findUser(user);
			int id;
			if (u == null) {
				id = 14901;
			} else {
				id = u.getId();
			}
			if (notify) {
				ChatNotificationManager.INSTANCE.addNotification(
						getApplicationContext(), id, user, msg.getMessage());
			}
		}
	}

	public class ServiceBinder extends Binder {

		public XmppService getService() {
			return XmppService.this;
		}
	}
}
