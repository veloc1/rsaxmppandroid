package ru.veloc1.rsachat.xmpp.receivers;

import java.util.ArrayList;

import ru.veloc1.rsachat.adapters.UserAdapter;
import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.data.User;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class UsersReceiver extends BroadcastReceiver {

	private UserAdapter	mAdapter;

	public UsersReceiver(UserAdapter adapter) {
		mAdapter = adapter;
	}

	@Override
	public void onReceive(Context arg0, Intent intent) {
		ArrayList<User> res = intent.getParcelableArrayListExtra(XmppService.USERS);
		mAdapter.setItems(res);
	}

}
