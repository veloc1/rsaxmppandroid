package ru.veloc1.rsachat.xmpp.receivers;

import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.xmpp.XmppService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;


public class OnlineStatusReceiver extends BroadcastReceiver {

	private TextView	mTextView;
	private OnlineStatusRunnable	mRunnable;

	public OnlineStatusReceiver(TextView text) {
		mTextView = text;
	}

	public OnlineStatusReceiver(OnlineStatusRunnable runnable) {
		mRunnable = runnable;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getIntExtra(XmppService.STATUS_ONLINE, 0) == 1) {
			if (mTextView != null){
				mTextView.setText(context.getString(R.string.online));
			}
		}
		else if (intent.getIntExtra(XmppService.STATUS_ONLINE, 0) == -1){
			if (mTextView != null){
				mTextView.setText(context.getString(R.string.error));
			}
		}
		else {
			if (mTextView != null){
				mTextView.setText(context.getString(R.string.offline));
			}
		}
		if (mRunnable != null){
			mRunnable.setArguments(intent.getIntExtra(XmppService.STATUS_ONLINE, 0));
			mRunnable.run();
		}
	}

	public static abstract class OnlineStatusRunnable implements Runnable{

		protected int	mStatus;

		public void setArguments(int status){
			mStatus = status;
		}
		
		@Override
		public abstract void run();
		
	}
	
}
