package ru.veloc1.rsachat.xmpp.receivers;

import ru.veloc1.rsachat.R;
import ru.veloc1.rsachat.xmpp.XmppService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


public class AddedUserStatusReceiver extends BroadcastReceiver {


	private Context	mContext;

	public AddedUserStatusReceiver(Context contextToToast) {
		mContext = contextToToast;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getBooleanExtra(XmppService.ADDED, false)) {
		}
		else {
			Toast.makeText(mContext, R.string.unable_to_add_user, Toast.LENGTH_SHORT).show();
		}
	}

}
