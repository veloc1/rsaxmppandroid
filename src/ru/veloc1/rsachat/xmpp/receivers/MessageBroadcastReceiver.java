package ru.veloc1.rsachat.xmpp.receivers;

import ru.veloc1.rsachat.xmpp.XmppService;
import ru.veloc1.rsachat.xmpp.data.Message;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class MessageBroadcastReceiver extends BroadcastReceiver {

	private MessageBroadcastRunnable	mRunnable;

	public MessageBroadcastReceiver(MessageBroadcastRunnable runnable) {
		mRunnable = runnable;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		mRunnable.setMessage((Message) intent.getParcelableExtra(XmppService.MESSAGE));
		mRunnable.run();
	}

	public static abstract class MessageBroadcastRunnable implements Runnable {

		protected Message message;

		public void setMessage(Message msg){
			message = msg;
		}
		
		@Override
		public abstract void run();

	}

}
