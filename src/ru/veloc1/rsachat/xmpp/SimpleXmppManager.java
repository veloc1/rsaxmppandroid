package ru.veloc1.rsachat.xmpp;

import java.util.ArrayList;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.Roster.SubscriptionMode;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;

import ru.veloc1.rsachat.xmpp.data.User;
import ru.veloc1.rsachat.xmpp.lowlevel.ServiceHandler;
import android.util.Log;


public enum SimpleXmppManager {

	INSTANCE;

//	private static final String	HOST		= "192.168.43.59";
	private static final String	HOST		= "jabber.ru";
	private static final int	PORT		= 5222;
//	public static final String	SERVICE		= "localhost";
	public static final String	SERVICE		= "jabber.ru";

	private XMPPConnection		mConnection;
	public boolean				mDebugFlag	= true;
	private String				mLastUser;
	private String				mLastPassword;
	private XmppService mService;

	public int connect(final String user, final String password, XmppService service) {
		if (mDebugFlag) {
			Log.d(getClass().getSimpleName(), "Connect");
		}
		mLastUser = user;
		mLastPassword = password;
		mService = service;
		return reconnect();
	}

	private int reconnect() {
		ConnectionConfiguration connConfig = new ConnectionConfiguration(HOST, PORT, SERVICE);
		connConfig.setSelfSignedCertificateEnabled(true);
		mConnection = new XMPPConnection(connConfig);
		try {
			mConnection.connect();
			if (mDebugFlag) {
				Log.d(getClass().getSimpleName(), "Login");
			}
			mConnection.login(mLastUser, mLastPassword);
			if (mDebugFlag) {
				Log.d(getClass().getSimpleName(), "Set presence");
			}
			Presence presence = new Presence(Presence.Type.available);
			presence.setStatus("I'm online with veloc1's chat!");
			mConnection.sendPacket(presence);

			Roster.setDefaultSubscriptionMode(SubscriptionMode.accept_all);
			getRoster().setSubscriptionMode(Roster.SubscriptionMode.accept_all);

			return 1;
		}
		catch (XMPPException e) {
			if (mDebugFlag) {
				Log.d(getClass().getSimpleName(), "Exception!");
			}
			e.printStackTrace();
			return -1;
		}
	}

	public boolean isConnected() {
		return mConnection == null ? false : true;
	}

	public Roster getRoster() {
		if (isConnected()) {
			if (mDebugFlag) Log.d(getClass().getSimpleName(), "getRoster");
			return mConnection.getRoster();
		}
		return null;
	}

	public ArrayList<RosterEntry> getEntries() {
		if (isConnected()) {
			Roster r = getRoster();
			return new ArrayList<RosterEntry>(r.getEntries());
		}
		return new ArrayList<RosterEntry>();
	}

	public ArrayList<User> getUsers() {
		ArrayList<User> res = new ArrayList<User>();
		if (isConnected()) {
			Roster r = getRoster();
			ArrayList<RosterEntry> entries = new ArrayList<RosterEntry>(r.getEntries());
			for (RosterEntry e : entries) {
				boolean authorized = true;
				if (r.getPresence(e.getUser()).getType().equals(Presence.Type.unsubscribed)) {
					// r.getPresence(e.getUser()).setType(Presence.Type.available);
					Presence subscribe = new Presence(Presence.Type.subscribe);
					subscribe.setTo(e.getUser());
					mConnection.sendPacket(subscribe);
					authorized = false;
				}
				User u = new User(e.getName() == null ? e.getUser() : e.getName(), e.getUser(), r.getPresence(e.getUser()), authorized);
				res.add(u);
			}
		}
		return res;
	}

	public void disconnect() {
		if (mDebugFlag) Log.d(getClass().getSimpleName(), "Disconnect");
		if (isConnected()) {
			mConnection.disconnect();
		}
	}

	public boolean addUser(String jid) {
		if (isConnected()) {
			try {
				// TODO search
				// UserSearchManager search = new UserSearchManager(mConnection);
				// String searchStr = "search.".concat(mConnection.getHost());
				// Form searchForm = search.getSearchForm(searchStr);
				// Form answerForm = searchForm.createAnswerForm();
				// answerForm.setAnswer("Username", true);
				// answerForm.setAnswer("search", jid);
				// ReportedData data = search.getSearchResults(answerForm, searchStr);
				// if (data.getRows() != null) {
				getRoster().createEntry(jid, jid, null);
				return true;
				// }
				// return false;
			}
			catch (XMPPException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public void removeUserFromRoster(String jid) {
		if (isConnected()) {
			RosterEntry userToRemove = getRoster().getEntry(jid);
			try {
				getRoster().removeEntry(userToRemove);
			}
			catch (XMPPException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean sendMessage(String user, String message) {
		if (isConnected()) {
			ChatManager chatManager = mConnection.getChatManager();
			Chat chat = chatManager.createChat(user, new ServiceHandler.ChatMessageListener(mService));
			try {
				chat.sendMessage(message);
				return true;
			}
			catch (XMPPException e) {
				e.printStackTrace();

			}
		}
		return false;
	}

	public ChatManager getChatManager() {
		if (isConnected()) {
			return mConnection.getChatManager();
		}
		return null;
	}

	public String getSelfUser() {
		if (isConnected()){
			return mLastUser.concat("@".concat(SERVICE));
		}
		return "";
	}

	public boolean sendKey(String user, String key) {
		if (isConnected()) {
			ChatManager chatManager = mConnection.getChatManager();
			Chat chat = chatManager.createChat(user, new ServiceHandler.ChatMessageListener(mService));
			try {
				Message keyMessage = new Message();
				keyMessage.addBody(XmppService.MESSAGE, XmppService.KEY);
				keyMessage.addBody(XmppService.KEY, key);
				chat.sendMessage(keyMessage);
				return true;
			}
			catch (XMPPException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
